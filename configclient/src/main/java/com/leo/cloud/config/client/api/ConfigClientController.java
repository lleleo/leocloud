package com.leo.cloud.config.client.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 文件名：ConfigClientController.java
 *
 */
@RestController
public class ConfigClientController {
    @Value("${param}")
    String param;

    @GetMapping
    public String get(){
        System.out.println("configserver.param="+param);
        return "configclient-------get param from configserver-------->"+param;
    }

    @GetMapping("/hi")
    public String hi(HttpServletRequest request){
        System.out.println(request.getHeader("Hello"));
        return "configclient say hi ";
    }
}
