package com.leo.cloud.zipkclient.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 文件名：ConstructTool.java
 *
 */
@Component
public class ConstructTool {

    private Logger logger = LoggerFactory.getLogger(ConstructTool.class);

    @Value("${server.port}")
    private String serverPort;

    @PostConstruct
    public void initial2(){
        System.out.println("--------->PostConstruct2222222222222.....");// http://10.88.148.222:8083/bus/refresh
        logger.error("------------->\nhttp://127.0.0.1:"+serverPort+"/order/create");
        logger.error("------------->\nhttp://127.0.0.1:"+serverPort+"/search");
    }

    @PostConstruct
    public void initial(){
//        logger.error("------------->\nhttp://127.0.0.1:"+serverPort+"/"+path+"/");
        System.out.println("--------->PostConstruct.....");
    }


}
