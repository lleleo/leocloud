package com.leo.cloud.zipkclient.api;

import org.springframework.stereotype.Service;

/**
 * 文件名：CartService.java
 *
 * @create 2021-06-11 9:49
 * <p>
 */
@Service
public class CartService {
    public void addProductToCart(String id,String name){
        System.out.println("添加商品"+id+", name"+name);
    }
}
