package com.leo.cloud.zipkclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 文件名：ZipkClientApplication.java
 *
 * @create 2021-06-11 9:46
 * <p>
 */
@SpringBootApplication
@EnableEurekaClient
public class CartApplication {
    public static void main(String[] args) {
        SpringApplication.run(CartApplication.class,args);
    }
}
