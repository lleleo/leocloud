package com.leo.cloud.zipk.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 文件名：zipkclient2.java
 *
 * @create 2021-06-11 10:05
 * <p>
 */
@SpringBootApplication
@EnableEurekaClient
public class Zipkclient2Application {
    public static void main(String[] args) {
        SpringApplication.run(Zipkclient2Application.class,args);
    }
}
