package com.leo.cloud.zipk.client.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件名：ProviderController.java
 *
 * @create 2021-06-11 10:17
 * <p>
 */
@RestController
public class ProviderController {

    @GetMapping("provide")
    public String provide(){
        System.out.println("ProviderController invoked");
        return "provide";
    }
}
