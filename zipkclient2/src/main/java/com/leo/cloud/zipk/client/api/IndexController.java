package com.leo.cloud.zipk.client.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * 文件名：IndexController.java
 *
 * @create 2021-06-11 10:16
 * <p>
 */
@RestController
public class IndexController {
    @Value("${spring.application.name}")
    String appName;
    @Value("${server.servlet.context-path}")
    String contextPath;
    @Autowired
    private RestTemplate restTemplate;

    @GetMapping
    public String get(){
        System.out.println("appname="+appName);
        System.out.println("contextPath="+contextPath);
        String url;
        url="http://"+appName+"/"+contextPath+"/provide";
//        url="http://zipkprovider/"+appName+"/provide";
//        url="http://localhost:8092/provide";
        System.out.println(url);
        String s = restTemplate.getForEntity(url, String.class ).getBody();
        System.out.println("IndexController invoked, res="+s);
        return s;
    }
}
