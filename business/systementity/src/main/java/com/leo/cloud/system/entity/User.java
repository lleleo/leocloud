package com.leo.cloud.system.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel(value = "用户",description = "用户信息")
public class User implements Serializable {
    @ApiModelProperty(value = "用户id")
    private String userId;

    @ApiModelProperty(value = "用户登录id")
    private String userName;

    @ApiModelProperty(value = "姓名名称")
    private String userAlias;

}
