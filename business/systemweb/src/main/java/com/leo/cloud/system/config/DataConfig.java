package com.leo.cloud.system.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 文件名：DataConfig.java
 *
 * 配置数据
 */
@Configuration
@PropertySource("classpath:application.yml")
@ConfigurationProperties(prefix = "data")
@EnableConfigurationProperties
public class DataConfig {

    private List<String> list = new ArrayList<>();

    private Map<String, String> map = new HashMap<>();

    public List<String> getList() {
        return list;
    }

    public void setList(List<String> list) {
        this.list = list;
    }

    public Map<String, String> getMap() {
        return map;
    }

    public void setMap(Map<String, String> map) {
        this.map = map;
    }
}
