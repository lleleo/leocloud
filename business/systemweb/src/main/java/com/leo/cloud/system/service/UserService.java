package com.leo.cloud.system.service;

import com.alibaba.fastjson.JSON;
import com.leo.cloud.constant.R;
import com.leo.cloud.system.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public R save(User user){
        System.out.println("systemService.userSerive.save() "+ JSON.toJSONString(user));
        return R.success(null, userRepository.save(user));
    }

    public User get(String id){
        System.out.println("systemService.userSerive.get() "+ id);
        return userRepository.get(id);
    }
}
