package com.leo.cloud.system.web;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.leo.cloud.constant.R;
import com.leo.cloud.system.config.DataConfig;
import com.leo.cloud.system.entity.User;
import com.leo.cloud.system.service.UserService;
import com.netflix.discovery.converters.Auto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(value = "用户管理",tags = "用户管理tag")
@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private DataConfig dataConfig;

    @ApiOperation(value = "保存用户")
    @PostMapping("save")
    public R save(@RequestBody User user){
        R r =  userService.save(user);

        System.out.println("-------------systemService.user.save()--return Value---"+JSON.toJSON(r));

        return r;
    }

    @ApiOperation(value = "根据id获取用户")
    @ApiImplicitParam(name = "id",dataType = "String",value = "用户id",example = "123",required = true)
    @GetMapping("/get/{id}")
    public User get(@PathVariable(name = "id") String id){
        return userService.get(id);
    }

    @ApiOperation("分页查询用户")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "分页页码,从1开始",example = "1",required = true),
            @ApiImplicitParam(name = "size",value = "分页条数",example = "10",required = true)
    }
    )
    @GetMapping("page")
    public Page<User> page(@RequestParam(required = false,defaultValue = "1") int page,
                           @RequestParam(required = false,defaultValue = "10")int size){
        System.out.println("page="+page+"----------------"+size);
        return null;
    }

    @ApiOperation("map配置")
    @GetMapping("map")
    public Object map(){
        return dataConfig.getMap();
    }
    @ApiOperation("map配置")
    @GetMapping("list")
    public Object list(){
        return dataConfig.getList();
    }
}
