package com.leo.cloud.system.service;

import com.leo.cloud.system.entity.User;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public class UserRepository {
    public User get(String id){
        System.out.println("-----.-UserRepository.get-----"+id);
        return new User(UUID.randomUUID().toString(),"123",id);
    }
    public User save(User user){
        System.out.println("-----.-UserRepository.save-----");
        user.setUserId(UUID.randomUUID().toString());
        return user;
    }
}
