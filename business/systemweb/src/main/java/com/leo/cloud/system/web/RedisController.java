package com.leo.cloud.system.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 文件名：RedisController.java
 *
 *
 */
@Api(value = "redis",tags = "redisTag")
@RestController
@RequestMapping("redis")
public class RedisController {
    @Autowired
    private RedisTemplate redisTemplate;

    @ApiOperation("opsForValue")
    @GetMapping("opsForValue")
    public String opsForValue(){
        String k ="112233";
        redisTemplate.opsForValue().set(k, "324", 2*60*60*1000L);
        redisTemplate.opsForValue().set(k, "123", 2*60*60*1000L);

        System.out.println(redisTemplate.opsForValue().get(k+"2"));
        System.out.println(redisTemplate.opsForValue().get(k));
        System.out.println(redisTemplate.opsForValue().get(k));
        System.out.println(redisTemplate.opsForValue().get(k));
        System.out.println(redisTemplate.opsForValue().get(k));
        return "opsForValue";
    }


    @ApiOperation("opsForHash")
    @GetMapping("opsForHash")
    public String opsForHash(){
        String k1 ="hv";
        String absentTest="3ddf432";
        redisTemplate.opsForHash().put(k1, absentTest,"12df3");

        redisTemplate.opsForHash().putIfAbsent(k1, absentTest,"0000000");
        System.out.println("get  putIfAbsent="+redisTemplate.opsForHash().get(k1,absentTest));

        redisTemplate.opsForHash().put("hashValue","map1","map1-1");
        redisTemplate.opsForHash().put("hashValue","map2","map2-2");

        List<Object> hashList = redisTemplate.opsForHash().values("hashValue");
        System.out.println("通过values(H key)方法获取变量中的hashMap值:" + hashList);

        Map<Object,Object> map = redisTemplate.opsForHash().entries("hashValue");
        System.out.println("通过entries(H key)方法获取变量中的键值对:" + map);

        String k2 ="5676";
        Map<String,String> map1 = new HashMap<>();
        map1.put("hk1", "3sd4");
        map1.put("k2123", "344sd3");
        Map<String,String> map2 = new HashMap<>();
        map2.put("55d25", "3443" );
        redisTemplate.opsForHash().putAll(k1, map1);

        redisTemplate.opsForHash().put(k1, absentTest,"456");

        redisTemplate.opsForHash().putIfAbsent(k1, absentTest,"0000000");
        System.out.println("get  putIfAbsent="+redisTemplate.opsForHash().get(k1,absentTest));

        redisTemplate.opsForHash().putAll(k1, map2);

        redisTemplate.opsForHash().put(k2, "123", "3443");

        System.out.println(redisTemplate.opsForHash().get(k1,"hk1"));
        System.out.println(redisTemplate.opsForHash().get(k1,"3432"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));
        System.out.println(redisTemplate.opsForHash().get(k1,"k2123"));

        System.out.println(redisTemplate.opsForHash().get(k2,"123"));
        return "opsForHash";
    }


    @ApiOperation("opsForList")
    @GetMapping("opsForList")
    public String opsForList(){
        String k1 ="ehert34";
        String k2 ="4f5676";

        redisTemplate.opsForList().rightPush(k1, "123");
        redisTemplate.opsForList().rightPush(k1, "456");
        System.out.println(redisTemplate.opsForList().leftPop(k1));
        System.out.println(redisTemplate.opsForList().leftPop(k1));
        System.out.println(redisTemplate.opsForList().leftPop(k1));
        System.out.println(redisTemplate.opsForList().leftPop(k2));

        redisTemplate.opsForList().rightPush(k2, "123");
        redisTemplate.opsForList().rightPush(k2, "1234");
        System.out.println(redisTemplate.opsForList().leftPop(k2));
        System.out.println(redisTemplate.opsForList().leftPop(k2));
        System.out.println(redisTemplate.opsForList().leftPop(k2));

        return "opsForList";
    }


    @ApiOperation("opsForSet")
    @GetMapping("opsForSet")
    public String opsForSet(){
        String k1 ="fgds";
        redisTemplate.opsForSet().add(k1,"dfg","112","131","114","114","11");
        System.out.println(redisTemplate.opsForSet().size(k1));
        redisTemplate.opsForSet().add(k1,"11","112","141");
        System.out.println(redisTemplate.opsForSet().size(k1));

        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        System.out.println(redisTemplate.opsForSet().pop(k1));
        return "opsForSet";
    }

}
