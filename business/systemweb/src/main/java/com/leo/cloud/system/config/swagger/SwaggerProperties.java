package com.leo.cloud.system.config.swagger;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 文件名：SwaggerProperties.java
 *
 *
 */
@Data
@Component
@ConfigurationProperties(prefix = "nm.swagger")
public class SwaggerProperties {
    private Boolean enable;
    private String apiBasePackage;
    private String title;
    private String[] description;
    private String version;
    private String contactName;
    private String contactUrl;
    private String contactEmail;
}
