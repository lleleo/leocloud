package com.leo.cloud.system.config.swagger;

import io.swagger.annotations.ApiOperation;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * 文件名：SwaggerConfig.java
 *
 *
 */
@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
@ConditionalOnProperty(prefix = "nm.swagger",value = {"enable"},havingValue = "true")
public class SwaggerConfig {
    @Autowired
    private SwaggerProperties swaggerProperties;
    @Bean
    public Docket api() {
        System.out.println("swaggerProperties="+ swaggerProperties.getEnable());
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .paths(PathSelectors.any())
                .build().enable(true)
                //主要关注点--统一填写一次token
                .apiInfo(apiInfo())
                .securitySchemes(security())
                .securityContexts(securityContexts());
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerProperties.getTitle())
                .description(StringUtils.join(swaggerProperties.getDescription()))
                .version(swaggerProperties.getVersion())
                .contact(new Contact(swaggerProperties.getContactName(),swaggerProperties.getContactUrl(),swaggerProperties.getContactEmail()))
                .build();
    }


    private List<ApiKey> security() {
        ApiKey key = new ApiKey("AUTHORIZATION", "AUTHORIZATION", "header");
        List<ApiKey> keys = new ArrayList<>();
        keys.add(key);
        return keys;
    }

    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts = new ArrayList<>();
        securityContexts.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .forPaths(PathSelectors.regex("^(?!auth).*$"))
                        .build());
        return securityContexts;
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences = new ArrayList<>();
        securityReferences.add(new SecurityReference("AUTHORIZATION", authorizationScopes));
        return securityReferences;
    }
}
