package com.leo.cloud.client.websocket;

import lombok.Data;

/**
 * 文件名：MyMessage.java
 *
 */
@Data
public class MyMessage {
    private String userId;
    private String message;
}
