package com.leo.cloud.client.config.swagger;

/**
 * 文件名：SwaggerTagConst.java
 * @create 2020-05-08 14:18
 * @create by Dibbing pan
 *
 * @Description: swaggerTAG枚举类
 */
public class SwaggerTagConst {
    public static class SYSTEM {
        public static final String HELLO = "测试接口";
        public static final String INDEX = "门户接口";
    }
    /**
     *  第三方系统管理
     */
    public static class FLOW {
        public static final String FLOW="流程相关接口";
        public static final String PROCESS_CONFIG="流程定义接口";
        public static final String EXPORT="导出";
    }
}
