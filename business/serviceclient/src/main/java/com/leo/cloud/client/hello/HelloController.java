package com.leo.cloud.client.hello;

import com.leo.cloud.client.config.swagger.SwaggerTagConst;
import com.leo.cloud.constant.R;
import com.leo.cloud.system.entity.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;

@Api(value = "test",tags = SwaggerTagConst.SYSTEM.HELLO, description = "调用测试")
@RestController
public class HelloController {
    @Value("${spring.profiles.active}")
    String profile;
    @Value("${app.value}")
    String appValue;

    @Value("${server.port}")
    String port;

    @Autowired
    private HelloService helloService;

    @GetMapping
    public void index(HttpServletResponse response)throws Exception{
        response.sendRedirect("/index.html");
    }

    @ApiOperation("FeignClient调用")
    @GetMapping("feignClient")
    public R saveUser(){
        return helloService.saveUser(new User("1",profile,appValue));
    }

    @Autowired
    private RestTemplate restTemplate;

    @ApiOperation("restTemplate.getForEntity调用")
    @GetMapping("hello")
    public User get(){
//        String order = restTemplate.getForObject("http://waiter-service/order/{id}", String.class, id);

        User user = restTemplate.getForEntity("http://system-provider/user/get/123", User.class).getBody();

//        String order = restTemplate.getForObject("http://localhost:8082/",String.class);

        System.out.println(user);
        return user;
    }
}