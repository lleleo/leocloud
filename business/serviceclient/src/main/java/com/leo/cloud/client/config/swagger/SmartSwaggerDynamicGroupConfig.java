package com.leo.cloud.client.config.swagger;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.leo.cloud.constant.Constant;
import io.swagger.annotations.Api;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.RequestHandler;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * [ 根据SwaggerTagConst内部类动态生成Swagger group ]
 */
@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
@Profile({"dev", "test", "prod"})
//@ConditionalOnProperty(prefix = "nm.swagger",value = {"enable"},havingValue = "true")
public class SmartSwaggerDynamicGroupConfig implements EnvironmentAware, BeanDefinitionRegistryPostProcessor {
    private int groupIndex = 0;
    private String groupName = "default";
    private List<String> groupList = Lists.newArrayList();
    private Map<String, List<String>> groupMap = Maps.newHashMap();
    private SwaggerProperties swaggerProperties;

    @Override
    public void setEnvironment(Environment environment) {
        swaggerProperties = new SwaggerProperties();
        swaggerProperties.setEnable(Boolean.parseBoolean(environment.getProperty("nm.swagger.enable")));
        swaggerProperties.setApiBasePackage(environment.getProperty("nm.swagger.apiBasePackage"));
        swaggerProperties.setTitle(environment.getProperty("nm.swagger.title"));
        swaggerProperties.setDescription(environment.getProperty("nm.swagger.description"));
        swaggerProperties.setVersion(environment.getProperty("nm.swagger.version"));
        swaggerProperties.setContactName(environment.getProperty("nm.swagger.contactName"));
        swaggerProperties.setContactUrl(environment.getProperty("nm.swagger.contactUrl"));
        swaggerProperties.setContactEmail(environment.getProperty("nm.swagger.contactEmail"));
        swaggerProperties.setServiceUrl(environment.getProperty("nm.swagger.serviceUrl"));
        swaggerProperties.setLicense(environment.getProperty("nm.swagger.license"));
        this.groupName=environment.getProperty("nm.swagger.groupName");
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        this.groupBuild();
        for (Map.Entry<String, List<String>> entry : groupMap.entrySet()) {
            String group = entry.getKey();
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(Docket.class, this :: baseDocket);
            BeanDefinition beanDefinition = builder.getRawBeanDefinition();
            registry.registerBeanDefinition(group + "Api", beanDefinition);
        }
    }

    private void groupBuild() {
        Class clazz = SwaggerTagConst.class;
        Class[] innerClazz = clazz.getDeclaredClasses();
        for (Class cls : innerClazz) {
            String group = cls.getSimpleName();
            List<String> apiTags = Lists.newArrayList();
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                boolean isFinal = Modifier.isFinal(field.getModifiers());
                if (isFinal) {
                    try {
                        apiTags.add(field.get(null).toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            groupList.add(group);
            groupMap.put(group, apiTags);
        }
    }
    public Docket defaultApi2() {
        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(serviceApiInfo())
                .enable(swaggerProperties.getEnable())
                //分组名称
                .groupName(groupName)
                .forCodeGeneration(true)
                .select()
                //这里指定Controller扫描包路径
                .apis(RequestHandlerSelectors.basePackage(swaggerProperties.getApiBasePackage()))
                .apis(getControllerPredicate())
                .paths(PathSelectors.any())
                .build()
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
        return docket;
    }
    public Docket baseDocket() {
        // 请求类型过滤规则
        Predicate<RequestHandler> controllerPredicate = getControllerPredicate();
        // controller 包路径
        Predicate<RequestHandler> controllerPackage = RequestHandlerSelectors.basePackage(swaggerProperties.getApiBasePackage());
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(groupName)
                .enable(swaggerProperties.getEnable())
                .forCodeGeneration(true)
                .select()
                .apis(controllerPackage)
                .apis(controllerPredicate)
                .paths(PathSelectors.any())
                .build()
                .apiInfo(this.serviceApiInfo())
                .securitySchemes(securitySchemes())
                .securityContexts(securityContexts());
    }

    private List<ApiKey> securitySchemes() {
        List<ApiKey> apiKeyList= new ArrayList<>();
        apiKeyList.add(new ApiKey("x-access-token", Constant.USER_ID_HTTP_HEAD_KEY, "header"));
        return apiKeyList;
    }

    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts=new ArrayList<>();
        securityContexts.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .forPaths(PathSelectors.any())
                        .build());
        return securityContexts;
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences=new ArrayList<>();
        securityReferences.add(new SecurityReference("x-access-token", authorizationScopes));
        return securityReferences;
    }

    private Predicate<RequestHandler> getControllerPredicate() {
        if(groupList.size()<groupIndex){
            return null;
        }
        groupName = groupList.get(groupIndex);
        List<String> apiTags = groupMap.get(groupName);
        Predicate<RequestHandler> methodPredicate = (input) -> {
            Api api = null;
            Optional<Api> apiOptional = input.findControllerAnnotation(Api.class);
            if (apiOptional.isPresent()) {
                api = apiOptional.get();
            }
            List<String> tags = Arrays.asList(api.tags());
            if (api != null && apiTags.containsAll(tags)) {
                return true;
            }
            return false;
        };
        groupIndex++;
        // 扫描注解了API的controller类
        return Predicates.and(RequestHandlerSelectors.withClassAnnotation(Api.class), methodPredicate);
    }

    private ApiInfo serviceApiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerProperties.getTitle())
                .description(swaggerProperties.getDescription())
                .version(swaggerProperties.getVersion())
                .license(swaggerProperties.getLicense())
                .contact(new Contact(swaggerProperties.getContactName(), swaggerProperties.getContactUrl(), swaggerProperties.getContactEmail()))
                .termsOfServiceUrl(swaggerProperties.getServiceUrl())
                .build();
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {

    }
}
