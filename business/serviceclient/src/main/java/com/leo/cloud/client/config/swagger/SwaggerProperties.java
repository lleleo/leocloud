package com.leo.cloud.client.config.swagger;

import lombok.Data;
import lombok.ToString;

/**
 * 文件名：SwaggerProperties.java
 *
 *
 */
@Data
@ToString
public class SwaggerProperties {
    private Boolean enable;
    private String apiBasePackage;
    private String title;
    private String description;
    private String version;
    private String contactName;
    private String contactUrl;
    private String contactEmail;
    private String serviceUrl;
    private String license;
    private String groupName;
}
