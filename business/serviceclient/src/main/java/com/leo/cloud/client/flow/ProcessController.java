package com.leo.cloud.client.flow;

import com.leo.cloud.client.config.swagger.SwaggerTagConst;
import com.leo.cloud.constant.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件名：ProcessController.java
 *
 * @create 2021-06-30 14:49
 */
@Api(value = "flow",tags = SwaggerTagConst.FLOW.PROCESS_CONFIG, description = "流程配置接口")
@RestController("process")
public class ProcessController {
    @ApiOperation("分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "页码,从1开始",required = true,dataType = "int",example = "1"),
            @ApiImplicitParam(name = "size",value = "数据数",required = true,dataType = "integer",example = "10"),
    })
    @GetMapping("/page")
    public R get(int page,int size){
        return R.success(null,size);
    }
}
