package com.leo.cloud.client.flow;

import com.leo.cloud.client.config.swagger.SwaggerTagConst;
import com.leo.cloud.constant.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * 文件名：FlowController.java
 *
 */
@Api(value = "flow",tags = SwaggerTagConst.FLOW.FLOW, description = "流程相关接口")
@RestController("flow")
public class FlowController {
    @ApiOperation("启动流程")
    @PostMapping("/start")
    @ApiImplicitParam(name = "flowId",value = "流程id",required = true)
    @ResponseBody
    public R start(String flowId){
        return R.success(flowId, UUID.randomUUID().toString());
    }

    @ApiOperation("审批流程")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "logId",value = "流程记录id",required = true),
            @ApiImplicitParam(name = "pass",value = "是否通过",required = true,dataType = "boolean",example = "true"),
            @ApiImplicitParam(name = "opinion",value = "意见",required = true,example = "通过!")
    })
    @PostMapping("/approve")
    @ResponseBody
    public R approve(String logId,boolean pass,String opinion){
        return R.success(logId, UUID.randomUUID().toString());
    }
}
