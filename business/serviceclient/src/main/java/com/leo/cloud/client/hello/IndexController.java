package com.leo.cloud.client.hello;

import com.leo.cloud.client.config.swagger.SwaggerTagConst;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 文件名：IndexController.java
 *
 * @create 2021-06-30 14:43
 */
@Api(value = "index",tags = SwaggerTagConst.SYSTEM.INDEX,description = "调用测试")
@RestController("portal")
public class IndexController {
    @GetMapping("portal")
    public String string(){
        return "INDEXCCC";
    }
}
