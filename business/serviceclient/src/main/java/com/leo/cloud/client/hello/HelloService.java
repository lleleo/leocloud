package com.leo.cloud.client.hello;

import com.leo.cloud.constant.R;
import com.leo.cloud.system.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

//name 为product项目中application.yml配置文件中的application.name;
//path 为product项目中application.yml配置文件中的context.path;
@FeignClient(name = "system-provider",path ="/" )
//@Componet注解最好加上，不加idea会显示有错误，但是不影响系统运行；
@Component
public interface HelloService {

    @PostMapping(value = "/user/save")
    R saveUser(@RequestBody User user);
}