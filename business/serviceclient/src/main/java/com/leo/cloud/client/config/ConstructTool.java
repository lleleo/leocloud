package com.leo.cloud.client.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 文件名：ConstructTool.java
 *
 * @create 2018-05-05 11:13
 */
@Component
public class ConstructTool {

    private Logger logger = LoggerFactory.getLogger(ConstructTool.class);

    @Value("${server.port}")
    private String serverPort;

    @PostConstruct
    public void initial2(){
        System.out.println("--------->PostConstruct2222222222222.....");
        logger.error("------------->\nhttp://localhost:"+serverPort+"/swagger-ui.html");
        logger.error("------------->\nhttp://localhost:"+serverPort+"/doc.html");
        logger.error("------------->\nhttp://localhost:"+serverPort+"/");
    }

    @PostConstruct
    public void initial(){
        System.out.println("--------->PostConstruct.....");
    }


}
