<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<style type="text/css">
    .msg-body {
        color: chocolate;
    }
</style>
<body>
<div id="container"></div>
</body>

<script>
    var eventSource = new EventSource('http://localhost:8099/events/111');


    var insertFn = function (frm,msg) {
        var container = document.getElementById('container');
        var div = document.createElement('div');
        div.innerHTML = '<span class="msg-body">来自' + frm + '的消息 : </span><span>' + msg + '</span>';
        container.appendChild(div);

    };


    eventSource.addEventListener("message", function (evt) {
        var data = evt.data;

        var json = JSON.parse(data);

        insertFn(json.from,json.payload.timestamp)


        // insertFn(timestamp)
    });
    eventSource.addEventListener('error', function (ev) {
        console.log('sse server disconnected')
    })


</script>

</html>