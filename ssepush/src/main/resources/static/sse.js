/**
 * Created with IntelliJ IDEA.
 * Author: Dax
 * Description:
 * Date: 2019/8/22
 * Time: 18:36
 */

// 声明客户端连接 EventSource
var eventSource = new EventSource('http://localhost:8099/events/111');

// 默认的监听器  当然你可以在服务端声明定制化的监听器
eventSource.addEventListener("message", function (evt) {
    var data = evt.data;
//todo 你的逻辑
    var json = JSON.parse(data);
    console.log(json)
});
