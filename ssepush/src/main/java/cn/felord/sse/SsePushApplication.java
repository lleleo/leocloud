package cn.felord.sse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsePushApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsePushApplication.class, args);
    }

}
