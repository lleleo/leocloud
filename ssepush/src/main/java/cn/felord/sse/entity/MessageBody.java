package cn.felord.sse.entity;

import lombok.Data;

/**
 * @author Dax
 * @since 13:51  2019/8/22
 */
@Data
public class MessageBody<T> {
    private long timestamp;

    private T payload;

    private String from;

    private String to;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }
}
