package com.leo.cloud.zipkserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import zipkin2.server.internal.EnableZipkinServer;

/**
 * 文件名：ZipkServerApplication.java
 *
 */
@SpringBootApplication
@EnableZipkinServer
@EnableEurekaClient
public class ZipkServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ZipkServerApplication.class,args);
    }
}
