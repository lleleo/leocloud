package com.leo.cloud.eureka.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableEurekaServer
@EnableEurekaClient
@EnableWebSecurity
public class EurekaSecurityApplication {
    public static void main(String[] args) {
        SpringApplication.run(EurekaSecurityApplication.class,args);
    }
}
