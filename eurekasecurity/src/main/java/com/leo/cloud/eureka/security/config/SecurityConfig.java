package com.leo.cloud.eureka.security.config;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity auth) throws Exception {
        super.configure(auth);
        //关闭CSRF支持。默认情况下会激活此功能
        auth.csrf().disable();
    }
}