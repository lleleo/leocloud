package com.leo.cloud.gateway.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 文件名：ConstructTool.java
 *
 */
@Component
public class ConstructTool {

    private Logger logger = LoggerFactory.getLogger(ConstructTool.class);

    @Value("${server.port}")
    private String serverPort;

    @Value("${server.http.port}")
    private int httpPort;

    @PostConstruct
    public void initial2(){
        System.out.println("--------->PostConstruct2222222222222.....");
        logger.error("------------->\nhttps://127.0.0.1:"+serverPort+"/code?randomStr="+System.currentTimeMillis());
        logger.error("------------->\nhttp://127.0.0.1:"+httpPort+"/code?randomStr="+System.currentTimeMillis());
        logger.error("------------->\nhttp://127.0.0.1:"+httpPort+"/hi");
        logger.error("------------->\nhttp://127.0.0.1:"+httpPort+"/zipkinclient/add/123");
        logger.error("------------->\nhttp://127.0.0.1:"+httpPort+"/gatewayclient/s1?k=12");
        logger.error("------------->\nhttp://127.0.0.1:"+httpPort+"/serviceprovider/");
        logger.error("------------->\nhttp://127.0.0.1:"+httpPort+"/serviceprovider/oauth/token");

        logger.error("-----配置过滤器-------->\nhttp://127.0.0.1:"+httpPort+"/configclient/hi");
    }

    @PostConstruct
    public void initial(){
        System.out.println("--------->PostConstruct.....");
    }

}
