package com.leo.cloud.gateway.api;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Desc  网关断路器
 */
@Api(value = "Hystrix降级",description = "Hystrix",tags = "Hystrix")
@RestController
public class FallbackController {

    /*
     * @ClassName FallbackController
     * @Desc TODO   网关断路器
     * @Date 2019/6/23 19:35
     * @Version 1.0
     */
    @RequestMapping("/fallback")
    public String fallback() {
        return "I'm Spring Cloud Gateway fallback.";
    }


    @GetMapping("/oauth/token")
    public String token(){
        return cn.hutool.core.lang.UUID.fastUUID().toString();
    }
}
