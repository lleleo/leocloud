//package com.leo.cloud.gateway.config.swagger;
//
//import com.leo.test.gateway.swagger.FilterIgnorePropertiesConfig;
//import lombok.AllArgsConstructor;
//import org.springframework.cloud.gateway.route.RouteDefinition;
//import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
//import org.springframework.cloud.gateway.support.NameUtils;
//import org.springframework.context.annotation.Primary;
//import org.springframework.stereotype.Component;
//import springfox.documentation.swagger.web.SwaggerResource;
//import springfox.documentation.swagger.web.SwaggerResourcesProvider;
//
//import java.util.ArrayList;
//import java.util.Comparator;
//import java.util.List;
//import java.util.stream.Collectors;
//
//
///**
// * 注册中心发现服务的地方
// * SwaggerProvider这个类可以从注册中心拉取服务列表 我们通过配置 将注册服务的路由转换成swagger-api
// */
//@Component
//@Primary
//@AllArgsConstructor
//public class SwaggerProvider implements SwaggerResourcesProvider {
//    public static final String API_URI = "/v2/api-docs";
//	private final RouteDefinitionRepository routeDefinitionRepository;
//	private final FilterIgnorePropertiesConfig filterIgnorePropertiesConfig;
//
//	@Override
//    public List<SwaggerResource> get() {
//		List<SwaggerResource> resources = new ArrayList<>();
//		List<RouteDefinition> routes = new ArrayList<>();
//		routeDefinitionRepository.getRouteDefinitions().subscribe(route -> routes.add(route));
//		routes.forEach(routeDefinition -> routeDefinition.getPredicates().stream()
//			.filter(predicateDefinition -> "Path".equalsIgnoreCase(predicateDefinition.getName()))
//			.filter(predicateDefinition -> !filterIgnorePropertiesConfig.getSwaggerProviders().contains(routeDefinition.getId()))
//			.forEach(predicateDefinition -> resources.add(swaggerResource(routeDefinition.getId(),
//				predicateDefinition.getArgs().get(NameUtils.GENERATED_NAME_PREFIX + "0").replace("/**", API_URI))
//					)
//			)
//		);
//
//		return resources.stream().sorted(Comparator.comparing(SwaggerResource::getName))
//			.collect(Collectors.toList());
//    }
//
//    private SwaggerResource swaggerResource(String name, String location) {
//        SwaggerResource swaggerResource = new SwaggerResource();
//        swaggerResource.setName(name);
//        swaggerResource.setLocation(location);
//        swaggerResource.setSwaggerVersion("1.0");
//        return swaggerResource;
//    }
//}
