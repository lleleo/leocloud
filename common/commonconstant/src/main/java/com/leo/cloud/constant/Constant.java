package com.leo.cloud.constant;

import java.io.File;

public class Constant {

    public static final String USER_ID_HTTP_HEAD_KEY= "USER_ID";// 回调地址token的参数名称

    public static final int successCode = 1;

    public static final int failCode = -1;

    public static String fileRootpath;

    public static final String ENV_PROD_KEY = "prod";
    static {
        fileRootpath = System.getProperties( ).getProperty("user.dir") + File.separator + "files" + File.separator;
    }
}
