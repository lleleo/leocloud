package com.leo.cloud.constant;

import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 文件名： IDGenerater
 * <p>
 * 主键生成器
 */
public class IDGenerater {
    private static long uid = 0L;
    private static Lock lock = new ReentrantLock(true);// 公平锁，先到先得

    public static long genId() {
        lock.lock();
        try {
            long id;
            do
                id = System.currentTimeMillis();
            while (id == uid);
            uid = id;
            return id;
        } finally {
            lock.unlock();
        }
    }
    public static String genIdStr(){
        return UUID.randomUUID().toString().replace("-","").toLowerCase();
    }

    public static void main(String[] args) {
        System.out.println(genIdStr());
    }

}
