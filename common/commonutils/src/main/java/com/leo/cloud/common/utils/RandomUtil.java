package com.leo.cloud.common.utils;

import java.util.Random;

/**
 * 文件名：RandomUtil.java
 * @create 2018-09-04 15:11
 * @create by Dibbing pan 
 *
 * @Description: 随机工具类
 */
public class RandomUtil {
    public static void main(String[] args) {
        System.out.println(alpha(6));
    }
    private static final Random RANDOM = new Random();
    //定义验证码字符.去除了O和I等容易混淆的字母
    private static final char ALPHA[]={'A','B','C','D','E','F','G','H','G','K','M','N','P','Q','R','S','T','U','V','W','X','Y','Z'
            ,'a','b','c','d','e','f','g','h','i','j','k','m','n','p','q','r','s','t','u','v','w','x','y','z','2','3','4','5','6','7','8','9'};

    /**
     * 产生两个数之间的随机数
     * @param min 小数
     * @param max 比min大的数
     * @return int 随机数字
     */
    private static int num(int min, int max)
    {
        return min + RANDOM.nextInt(max - min);
    }

    /**
     * 产生0--num的随机数,不包括num
     * @param num 数字
     * @return int 随机数字
     */
    private static int num(int num)
    {
        return RANDOM.nextInt(num);
    }
    /**
     * 生成size长度的字符串
     */
    public static String alpha(int size){
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            builder.append(ALPHA[num(0, ALPHA.length)]);
        }
        return builder.toString();
    }
}