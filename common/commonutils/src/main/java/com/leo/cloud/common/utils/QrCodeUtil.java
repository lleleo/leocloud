package com.leo.cloud.common.utils;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件名：QrCodeUtil.java
 *
 * @create 2020-12-04 14:40
 * <p>
 */
public class QrCodeUtil {

    private static final int BLACK = 0xFF000000;
    private static final int WHITE = 0xFFFFFFFF;


    public static final int DKGRAY = 0xFF444444; // 深灰
    public static final int GRAY = 0xFF888888;
    public static final int LTGRAY = 0xFFCCCCCC; // 浅灰
    public static final int RED = 0xFFFF0000;
    public static final int GREEN = 0xFF00FF00;
    public static final int BLUE = 0xFF0000FF;
    public static final int YELLOW = 0xFFFFFF00;
    public static final int CYAN = 0xFF00FFFF; // 浅蓝色
    public static final int MAGENTA = 0xFFFF00FF; // 紫色
    public static final int TRANSPARENT = 0;

    private static final int DEFAULT_HEIGHT = 85;// 二维码图片宽度
    private static final int DEFAULT_WIDTH = 85;// 二维码图片高度
    private static final String DEFAULT_FORMAT = "gif";// 二维码的图片格式

    private static final int BARCODE_HEIGHT = 30;
    private static final int BARCODE_WIDTH = 150;
    private static final String BARCODE_FORMAT = "png";

    /**
     * 生成二维码 到指定 路径 、这个方法二维码会返回到你请求的位置，通过response响应，
     * 上面的控制层方法调用的是该方法，如果不用返回到请求的位置，那么就使用下面的方法，可以输出到你想要到的地方
     *
     * @param outputStream
     *            到指定 路径 、FILE
     * @param text
     *            可以是网址 和 其他，网址就是跳转
     * @throws WriterException
     * @throws IOException
     */

    public static void encode2DCode(OutputStream outputStream, String text,int width,int height)
            throws WriterException, IOException {

        Integer onColor = 0xFF000000;  //前景色
        Integer offColor = 0xFFFFFFFF; //背景色
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); // 内容所使用字符集编码
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(text,
                BarcodeFormat.QR_CODE, width, height, hints);
        int bgColor = CYAN;
        int color = MAGENTA;
        BufferedImage image = MatrixToImageWriter.toBufferedImage(bitMatrix,new MatrixToImageConfig(color, bgColor));
        /*
         * Graphics2D gs = image.createGraphics(); //载入logo Image img =
         * ImageIO.read(new File(srcImagePath)); gs.drawImage(img, 125, 125,
         * null); gs.dispose(); img.flush();
         */
        if (!ImageIO.write(image, DEFAULT_FORMAT, outputStream)) {
            throw new IOException("Could not write an image of format " + DEFAULT_FORMAT + " to stream");
        }
    }
    public static BufferedImage encode2DCode(String text) {
        try {
            Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
            hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); // 内容所使用字符集编码
            hints.put(EncodeHintType.MARGIN, 1);
            BitMatrix bitMatrix = new MultiFormatWriter().encode(text,
                    BarcodeFormat.QR_CODE, DEFAULT_WIDTH, DEFAULT_HEIGHT, hints);

            return MatrixToImageWriter.toBufferedImage(bitMatrix);
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }
    /**
     * 生成二维码 到指定 路径 、二维码会输出到某一个位置
     *
     * @param outputFile
     *            到指定 路径 、FILE
     * @param text
     *            可以是网址 和 其他，网址就是跳转
     * @throws WriterException
     * @throws IOException
     */
    public static void encode2DCode(File outputFile, String text)
            throws WriterException, IOException {
        Map<EncodeHintType, Object> hints = new HashMap<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8"); // 内容所使用字符集编码
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, DEFAULT_WIDTH, DEFAULT_HEIGHT, hints);
        MatrixToImageWriter.writeToFile(bitMatrix, DEFAULT_FORMAT, outputFile);
    }
}
