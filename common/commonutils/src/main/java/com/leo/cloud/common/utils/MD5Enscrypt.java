package com.leo.cloud.common.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 文件名：MD5Enscrypt.java
 * @create 2020-04-28 15:15
 * @create by Dibbing pan
 *
 * @Description: MD5 算法
 */
public class MD5Enscrypt {
    // 全局数组
    private final static String[] strDigits = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

    public MD5Enscrypt() {
    }

    // 返回形式为数字跟字符串
    private static String byteToArrayString(byte bByte) {
        int iRet = bByte;
        // ThirdSystem.out.println("iRet="+iRet);
        if (iRet < 0) {
            iRet += 256;
        }
        int iD1 = iRet / 16;
        int iD2 = iRet % 16;
        return strDigits[iD1] + strDigits[iD2];
    }

    // 返回形式只为数字
    private static String byteToNum(byte bByte) {
        int iRet = bByte;
//        ThirdSystem.out.println("iRet1=" + iRet);
        if (iRet < 0) {
            iRet += 256;
        }
        return String.valueOf(iRet);
    }

    // 转换字节数组为16进制字串
    private static String byteToString(byte[] bByte) {
        StringBuffer sBuffer = new StringBuffer();
        for (int i = 0; i < bByte.length; i++) {
            sBuffer.append(byteToArrayString(bByte[i]));
        }
        return sBuffer.toString();
    }

    public static String GetMD5Code(String strObj) {
        String resultString = null;
        try {
            resultString = new String(strObj);
            MessageDigest md = MessageDigest.getInstance("MD5");
            // md.digest() 该函数返回值为存放哈希值结果的byte数组
            resultString = byteToString(md.digest(strObj.getBytes()));
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return resultString;
    }

    public static void main(String[] args) {
//    	JSONObject jsonObject = new JSONObject();
//    	jsonObject.put("systemCode", IDGenerater.genIdStr());
//    	jsonObject.put("pushTime", DateUtil.formatDateTime(new Date()));
//    	System.out.println(jsonObject.toJSONString());
//        System.out.println(MD5Enscrypt.GetMD5Code(jsonObject.toJSONString()));

        System.out.println(GetMD5Code("30000061|TGsW2Dp"));
        System.out.println(GetMD5Code("30000039|ShK651K"));
    }
}