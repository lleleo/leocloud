package com.leo.cloud.common.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 文件名：DateUtil.java
 */
public class DateUtil {
    public static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat monthFormat = new SimpleDateFormat("yyyyMM");
    public static SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    public static DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    public static DateTimeFormatter dateNameFormatter = DateTimeFormatter.ofPattern("yyyy年MM月dd日");
    public static String getTodayDateStr(){
        return dateFormat.format(new Date());
    }
    public static String getThisMonthStr(){
        return monthFormat.format(new Date());
    }
    public static Date parseDate(String str){
        Date date = null;
        try {
            date = dateFormat.parse(str);
        }catch (Exception e){}
        return date;
    }
    public static Date parseDateTime(String dateTimeStr){
        Date date = null;
        try {
            date = dateTimeFormat.parse(dateTimeStr);
        }catch (Exception e){}
        return date;
    }
    public static String formatDate(Date date){
        if(null == date){
            return null;
        }
        return dateFormat.format(date);
    }
    public static String formatTime(Date date){
        if(null == date){
            return null;
        }
        return timeFormat.format(date);
    }
    public static String formatDateTime(Date date){
        if(null == date){
            return null;
        }
        return dateTimeFormat.format(date);
    }
    public static LocalDateTime date2LocalDateTime(Date date){
        return LocalDateTime.parse(formatDateTime(date),dateTimeFormatter);
    }
    public static String getLastDayByStr(String dateStr){
        try {
            return getLastDayStr(dateFormat.parse(dateStr));
        }catch (Exception e){
            return null;
        }
    }
    public static int getTodayDaysOfMonth(){
        Date today = new Date();
        Calendar c= Calendar.getInstance();
        c.setTime(today);
        int days=c.get(Calendar.DAY_OF_MONTH);
        return days;
    }
    public static String getLastDayStr(Date date){
        return dateFormat.format(getLastDayByDate(date));
    }
    public static Date getLastDayByDate(Date date){
        return new Date(date.getTime()-86400000L);
    }
    public static int betweenDays(Date start,Date end){
        return (int) ((end.getTime() - start.getTime()) / (1000*3600*24));
    }
    public static void main(String[] args) {
        String s2="2021-04-17 08:40:00";

        String s = "2021-04-25 08:40:00";

        System.out.println(LocalDateTime.parse(s2, dateTimeFormatter).isBefore(LocalDateTime.parse(s, dateTimeFormatter)));

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(getLastDateOfMonth(localDateTime));
        System.out.println(getLastDateOfMonth(LocalDate.now()));
        System.out.println(localDateTime.getMonthValue());
        System.out.println(getMaxDayByYearMonth(2020,5));
        System.out.println(s.substring(0,10));
        System.out.println(DateUtil.parseDate(s.substring(0,10)));
        System.out.println(betweenDays(parseDate("2019-08-01"),parseDate("2019-08-20")));


    }
    public static int getMaxDayByYearMonth(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year - 1);
        calendar.set(Calendar.MONTH, month);
        return calendar.getActualMaximum(Calendar.DATE);
    }
    public static String getLastDateOfMonth(LocalDate localDate){
        String s = localDate.getYear()+"-";
        int month=localDate.getMonthValue();
        if(month<10){
            s=s+"0"+month;
        }else {
            s = s+ month;
        }
        return s+"-"+getMaxDayByYearMonth(localDate.getYear(),month-1);
    }

    public static String getLastDateOfMonth(LocalDateTime localDateTime){
        String s = localDateTime.getYear()+"-";
        int month=localDateTime.getMonthValue();
        if(month<10){
            s=s+"0"+month;
        }else {
            s = s+ month;
        }
        return s+"-"+getMaxDayByYearMonth(localDateTime.getYear(),month-1);
    }

}
