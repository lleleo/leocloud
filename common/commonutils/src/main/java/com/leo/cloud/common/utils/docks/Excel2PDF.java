package com.leo.cloud.common.utils.docks;

import com.aspose.cells.License;
import com.aspose.cells.PdfSaveOptions;
import com.aspose.cells.Workbook;
import com.leo.cloud.common.utils.Constant;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

public class Excel2PDF {

    public static void main(String[] args) {
//        String path="D:\\java\\idea_workspace\\test\\excel2pdf1\\pdf\\rest1616637147965.pdf";
//        System.out.println(path);
//        System.out.println(path.substring(0, path.lastIndexOf("."))+".txt");

        String resourcesDir = "d:\\pdf\\";
        String sourceFilePath=resourcesDir + "合同借阅申请.xlsx";
        String desFilePath = sourceFilePath.substring(0, sourceFilePath.lastIndexOf("."))+System.currentTimeMillis()+".pdf";
        excel2pdf(sourceFilePath,desFilePath);
        System.out.println(desFilePath);

        sourceFilePath=resourcesDir + "合同借阅申请.xls";
        desFilePath = sourceFilePath.substring(0, sourceFilePath.lastIndexOf("."))+System.currentTimeMillis()+".pdf";
        excel2pdf(sourceFilePath,desFilePath);
        System.out.println(desFilePath);

    }

    /**
     * 获取license 去除水印
     * @return
     */
    public static boolean getLicense() {
        boolean result = false;
        try {
//            InputStream is = AsposeForExcel2PDF.class.getClassLoader().getResourceAsStream("\\license.xml");
            InputStream is = new FileInputStream(Constant.fileRootpath+"docs"+File.separator+"License.xml");
            License license = new License();
            license.setLicense(is);
            result = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * excel 转为pdf 输出。
     *
     * @param sourceFilePath  excel文件
     */
    public static String excel2pdf(String sourceFilePath,String outPath){
        if (!getLicense()) { // 验证License 若不验证则转化出的pdf文档会有水印产生
            return null;
        }
        try {
            Workbook wb = new Workbook(sourceFilePath);// 原始excel路径
            if(StringUtils.isBlank(outPath)){
                outPath = sourceFilePath.substring(0, sourceFilePath.lastIndexOf("."))+".pdf";
            }
            FileOutputStream fileOS = new FileOutputStream(outPath);
            PdfSaveOptions pdfSaveOptions = new PdfSaveOptions();
            pdfSaveOptions.setOnePagePerSheet(true);


            int[] autoDrawSheets={3};
            //当excel中对应的sheet页宽度太大时，在PDF中会拆断并分页。此处等比缩放。
//            autoDraw(wb,autoDrawSheets);

            int[] showSheets={0};
            //隐藏workbook中不需要的sheet页。
            printSheetPage(wb,showSheets);
            wb.save(fileOS, pdfSaveOptions);
            fileOS.flush();
            fileOS.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return outPath;
    }


    /**
     * 设置打印的sheet 自动拉伸比例
     * @param wb
     * @param page 自动拉伸的页的sheet数组
     */
    public static void autoDraw(Workbook wb,int[] page){
        if(null!=page&&page.length>0){
            for (int i = 0; i < page.length; i++) {
                wb.getWorksheets().get(i).getHorizontalPageBreaks().clear();
                wb.getWorksheets().get(i).getVerticalPageBreaks().clear();
            }
        }
    }


    /**
     * 隐藏workbook中不需要的sheet页。
     * @param wb
     * @param page 显示页的sheet数组
     */
    public static void printSheetPage(Workbook wb,int[] page){
        for (int i= 1; i < wb.getWorksheets().getCount(); i++)  {
            wb.getWorksheets().get(i).setVisible(false);
        }
        if(null==page||page.length==0){
            wb.getWorksheets().get(0).setVisible(true);
        }else{
            for (int i = 0; i < page.length; i++) {
                wb.getWorksheets().get(i).setVisible(true);
            }
        }
    }

}


