//package com.leo.cloud.common.utils;
//
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.web.multipart.MultipartFile;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.*;
//import java.net.URLEncoder;
//
///**
// * 文件名：HttpRequestUploadFileUtil.java
// * @create 2019-11-22 10:13
// * @create by Dibbing pan
// *
// * @Description: http 文件上传工具类
// */
//public class HttpRequestUploadFileUtil {
//
//    /**
//     * 上传文件
//     * @return 文件路径
//     * @exception Exception
//     */
//    public static String UpLoadFile(MultipartFile file,String pathPrefix,String fileName) throws Exception {
//        String OriginalFilename = file.getOriginalFilename();
//        if(StringUtils.isBlank(fileName)){
//            // 未设置文件名, 则自定义一个文件名
//            String suffix;
//            if(OriginalFilename.lastIndexOf(".")== -1){
//                suffix = "jpg";
//            }else {
//                suffix = OriginalFilename.substring(OriginalFilename.lastIndexOf(".")+1, OriginalFilename.length());
//            }
//            fileName = IDGenerater.genId()+"." +suffix;
//        }
//        File targetFile = new File(pathPrefix, fileName);
//        targetFile.mkdirs();
////	    String fileName = file.getOriginalFilename();
////	    File targetFile = new File(path, fileName);
////	    //判断文件是否存在
////	    if(!targetFile.exists()){
////	    	targetFile.mkdirs();
////	    }else {
////	        //文件名称
////	        String temp = fileName.substring(0,fileName.lastIndexOf("."));
////	        //文件后缀名
////	        String suffix = fileName.substring(fileName.lastIndexOf("."), fileName.length());
////	        fileName = temp + "_"+ ThirdSystem.currentTimeMillis() +suffix;
////	        targetFile = new File(path, fileName);
////	        targetFile.mkdirs();
////	    }
//        //保存
//        try {
//            file.transferTo(targetFile);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return targetFile.getAbsolutePath();
//    }
//
//    /**
//     * 上传文件
//     * @return 文件路径
//     * @exception Exception
//     */
//    public static String UpLoadFile(MultipartFile file, String suffixType, HttpServletRequest request) throws Exception {
//        String fileUrl = "";
//        String path = request.getSession().getServletContext().getRealPath("upload");
//        String fileName = file.getOriginalFilename();
//        String suffix = fileName.substring(fileName.lastIndexOf("."), fileName.length());
//        fileName = System.currentTimeMillis() +suffix;
//        File targetFile = new File(path, fileName);
//        targetFile.mkdirs();
//        if(!suffixType.equals(suffix)){
//            throw new RuntimeException ("请上传标准格式文件！");
//        }
////	    String fileName = file.getOriginalFilename();
////	    File targetFile = new File(path, fileName);
////	    //判断文件是否存在
////	    if(!targetFile.exists()){
////	    	targetFile.mkdirs();
////	    }else {
////	        //文件名称
////	        String temp = fileName.substring(0,fileName.lastIndexOf("."));
////	        //文件后缀名
////	        String suffix = fileName.substring(fileName.lastIndexOf("."), fileName.length());
////	        fileName = temp + "_"+ ThirdSystem.currentTimeMillis() +suffix;
////	        targetFile = new File(path, fileName);
////	        targetFile.mkdirs();
////	    }
//        //保存
//        try {
//            file.transferTo(targetFile);
//            fileUrl = "upload"+File.separator+fileName;
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return fileUrl;
//    }
//
//    /**
//     * 文件下载(二进制流),传入文件名和文件路径
//     * @param request
//     * @param response
//     * @param name 下载文件名
//     * @param subUrl 子路径
//     * @return
//     * @throws Exception
//     */
//    public static String downLoadFile(HttpServletRequest request,
//                                      HttpServletResponse response, String name, String subUrl)
//            throws Exception {
//
//        String Path = Constant.fileRootpath + subUrl;
//        File file = new File(Path);
//        if (file.exists()) {
//            InputStream fis = null;
//            OutputStream toClient = null;
//            name = name.trim();
//            if (name.contains(".")) {
//                name = name.substring(0, name.lastIndexOf("."));
//            }
//
//            name = name + "." + getFileExtension(subUrl);
//            try {
//                fis = new BufferedInputStream(new FileInputStream(Path));
//                byte[] buffer = new byte[fis.available()];
//                fis.read(buffer);
//                // 清空response
//                response.reset();
//                String userAgent = request.getHeader("USER-AGENT");
//                // 设置response的Header
//                if (userAgent != null) {
//                    String lowuserAgent = userAgent.toLowerCase();
//                    if (lowuserAgent.contains("windows")) {
//                        // windows
//                        response.setHeader("content-disposition",
//                                "attachment;filename=" + URLEncoder.encode(name, "utf-8"));
//                    } else {
//                        // 非windows
//                        response.setHeader("content-disposition",
//                                "attachment;filename=" + new String(name.getBytes("utf-8"),
//                                        "iso8859-1"));
//                    }
//                } else {
//                    // windows
//                    response.setHeader("content-disposition",
//                            "attachment;filename="
//                                    + URLEncoder.encode(name, "utf-8"));
//                }
//                response.addHeader("Content-Length", "" + file.length());
//                toClient = new BufferedOutputStream(response.getOutputStream());
//                response.setContentType("application/octet-stream");
//
//                toClient.write(buffer);
//                toClient.flush();
//
//                fis.close();
//                toClient.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//
//            } finally {
//                try {
//                    if (fis != null) {
//                        fis.close();
//                    }
//                    if (toClient != null) {
//                        toClient.close();
//                    }
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//            return null;
//        } else {
//            return "file/NotFound";
//        }
//
//    }
//    /**
//     * 获取文件扩展名
//     *
//     * @param url
//     * @return
//     */
//    private static String getFileExtension(String url) {
//        if (url.lastIndexOf(".") != -1 && url.lastIndexOf(".") != 0) {
//            return url.substring(url.lastIndexOf(".") + 1);
//        } else {
//            return "";
//        }
//    }
//}
