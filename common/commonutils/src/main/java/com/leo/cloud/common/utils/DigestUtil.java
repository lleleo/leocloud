package com.leo.cloud.common.utils;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * 文件名：DigestUtil.java
 * @create 2020-04-28 15:14
 * @create by Dibbing pan
 *
 * @Description: MD5加密
 */
public class DigestUtil extends DigestUtils {

    /**
     * md5加盐加密
     *
     * @param salt
     * @param password
     * @return
     */
    public static String encryptPass(String password,String salt) {
        return DigestUtil.md5Hex(String.format(salt, password));
    }
    /**
     * MD5验证方法
     *
     * @param text 明文
     * @param key 密钥
     * @param md5 密文
     * @return true/false
     * @throws Exception
     */
    public static boolean verify(String text, String key, String md5) throws Exception {
        //根据传入的密钥进行验证
        String md5Text = md5(text, key);
        if(md5Text.equalsIgnoreCase(md5)) {
            return true;
        }

        return false;
    }
    /**
     * MD5方法
     *
     * @param text 明文
     * @param key 密钥
     * @return 密文
     * @throws Exception
     */
    public static String md5(String text, String key) {
        //加密后的字符串
        try {
            return md5Hex(text + key);
        }catch (Exception e){
            return null;
        }
//        ThirdSystem.out.println("MD5加密后的字符串为:encodeStr="+encodeStr);
    }

    public static void main(String[] args)throws Exception {

        String s=md5("1623142016014","Cuh4EKH4");
        System.out.println(s);

        String salt= "1689fb5af3f5456e967ef43788b3b6f4";// IDGenerater.genIdStr();
        String password="#17708166066.Why";// "zyab@FLOWapp2020";
        password="#17708166066.Why";
        String enscrypt=md5(password,salt);
        System.out.println("salt:"+salt);
        System.out.println("password:"+password);
        System.out.println("encryption:"+enscrypt);
        System.out.println("valid:"+verify(password,salt,enscrypt));

//        System.out.println("===================================");
//        String p="123456";
//        String s=IDGenerater.genIdStr();
//        String en=md5(p,s);
//        System.out.println(p);
//        System.out.println(s);
//        System.out.println(en);
//        System.out.println(verify(password,salt,enscrypt));
    }
}
