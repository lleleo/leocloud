package com.leo.cloud.common.utils;

import java.io.Serializable;

public class R implements Serializable {
    private int code;
    private String message;
    private Object data;
    public R(){}
    private R (int code,String message){
        this.code=code;
        this.message=message;
    }
    private R (int code,String message,Object data){
        this.code=code;
        this.message=message;
        this.data = data;
    }
    public static R success(){
        return R.success(null);
    }
    public static R success(String message){
        return R.success(message,null);
    }
    public static R success(String message,Object data){
        return new R(Constant.successCode,message, data);
    }
    public static R fail(String message){
        return R.fail(message,null);
    }
    public static R fail(String message,Object data){
        return new R(Constant.failCode,message,data);
    }
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
