package com.leo.cloud.common.utils;

import java.util.concurrent.TimeUnit;

/**
 * 文件名：TimeUtils.java
 *
 * @create 2021-01-15 10:20
 * <p>
 * <p>
 */
public class TimeUtils {
    /**
     * @return 当前毫秒数
     */
    public static long nowMs() {
        return System.currentTimeMillis();
    }

    /**
     * 当前毫秒与起始毫秒差
     * @param startMillis 开始纳秒数
     * @return 时间差
     */
    public static long diffMs(long startMillis) {
        return diffMs(startMillis, nowMs());
    }
    private static long diffMs(long startMillis,long endMillis) {
        return (endMillis - startMillis);
    }
    public static void main(String[] args) throws InterruptedException {
        final long startMs = TimeUtils.nowMs();

        TimeUnit.SECONDS.sleep(5); // 模拟业务代码

        System.out.println("timeCost: " + TimeUtils.diffMs(startMs));
    }
}
