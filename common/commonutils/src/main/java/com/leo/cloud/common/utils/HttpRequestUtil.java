//package com.leo.cloud.common.utils;
//
//import com.alibaba.fastjson.JSONObject;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//
//import javax.servlet.http.HttpServletRequest;
//import java.net.InetAddress;
//import java.net.UnknownHostException;
//import java.util.Enumeration;
//
///**
// * 文件名：HttpRequestUtil.java
// * @create 2020-04-28 15:15
// * @create by Dibbing pan
// *
// * @Description: 获取请求参数工具类
// */
//public class HttpRequestUtil {
//    public static String getParameters(HttpServletRequest req) {
//        Enumeration<String> enums = req.getParameterNames();
//        JSONObject parameter = new JSONObject();
//        while (enums.hasMoreElements()) {
//            String name = enums.nextElement();
//            parameter.put(name, req.getParameter(name));
//        }
//        return parameter.toJSONString();
//    }
//
//    public static HttpServletRequest getRequest() {
//        ServletRequestAttributes ra= (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
//        return ra.getRequest();
//    }
//    /**
//     * 获得域名
//     */
//    public static String getDomain(HttpServletRequest request) {
//        String path = request.getContextPath();
//        return request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path;
//    }
//
//    /**
//     * 获取IP地址
//     *
//     * @param request
//     * @return
//     * @throws Exception
//     */
//    public static String getRemoteIp(HttpServletRequest request) throws Exception {
//        try {
//            String ip = request.getHeader("x-forwarded-for");
//            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
//                ip = request.getHeader("Proxy-Client-IP");
//            }
//            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
//                ip = request.getHeader("WL-Proxy-Client-IP");
//            }
//            if (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip)) {
//                ip = request.getRemoteAddr();
//                if ("127.0.0.1".equals(ip) || "0:0:0:0:0:0:0:1".equals(ip)) {
//                    InetAddress inet = InetAddress.getLocalHost();
//                    ip = inet.getHostAddress();
//                }
//            }
//            if (!StringUtils.isEmpty(ip)) {
//                ip = ip.split(",")[0];
//            }
//            return ip;
//        } catch (Exception e) {
//            //throw new Exception("获取IP地址失败！", e);
//            return null;
//        }
//    }
//
//    // 服务器ip
//    public static String getHostAddress(){
//        try {
//            InetAddress ip = InetAddress.getLocalHost();
////            System.out.println("第二种方式"+ip.getHostAddress());
//            return ip.getHostAddress();
//        }catch (Exception e){
//            return null;
//        }
//    }
//    public static void main(String[] args) throws UnknownHostException {
////        InetAddress IP = InetAddress.getByName("DESKTP-MDC2C");                    //在我的电脑属性查看名称粘贴过来，此为我的虚拟名。
////        System.out.println("第一种方式"+IP);
//
//        InetAddress ip = InetAddress.getLocalHost();
//        System.out.println("第二种方式"+ip.getHostAddress());
//
//        byte[] addr= {10,10,21,(byte)168};                                                   //在电脑网络设置查看自己的IP地址，粘贴过来，此为我的虚拟地址。
//        InetAddress ip2=InetAddress.getByAddress(addr);
//        System.out.println("第三种方式"+ip2);
//
//        System.out.println(ip2.getHostName());
//        byte[] address=ip2.getAddress();
//        for(byte a:address) {
//            int b=(a<0)?(a+256):a;
//            System.out.println("常规方法"+b+".");
//        }
//    }
//
//
//}
