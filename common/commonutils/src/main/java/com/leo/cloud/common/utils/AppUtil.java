package com.leo.cloud.common.utils;

//import com.google.common.collect.Maps;
//import net.sf.cglib.beans.BeanMap;
//import java.util.ArrayList;
//import java.util.Map;
//import java.util.Set;
import org.apache.commons.lang3.StringUtils;

import java.net.InetAddress;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 文件名：AppUtil.java
 *
 * @create 2020-07-22 9:38
 * <p>
 */
public class AppUtil {
    /**
     * 将对象转换为map
     *
     * @param bean
     * @return
     */
//    public static <T> Map<String, Object> beanToMap(T bean) {
//        Map<String, Object> map = Maps.newHashMap();
//        if (bean != null) {
//            BeanMap beanMap = BeanMap.create(bean);
//            for (Object key : beanMap.keySet()) {
//                map.put(key.toString(), beanMap.get(key));
//            }
//
//            Set<String> keySet = map.keySet();
//            List<String> list = new ArrayList<>(keySet.size());
//            for (String s : keySet) {
//                if(null == map.get(s) || StringUtils.isBlank(String.valueOf(map.get(s)))){
//                    list.add(s);
//                }
//            }
//            for (String s : list) {
//                map.remove(s);
//            }
//        }
//        return map;
//    }

    /**
     * 将map转换为javabean对象
     *
     * @param map
     * @param bean
     * @return
     */
//    public static <T> T mapToBean(Map<String, Object> map, T bean) {
//        BeanMap beanMap = BeanMap.create(bean);
//        beanMap.putAll(map);
//        return bean;
//    }

    public static boolean isNotEmptyList(List list){
        return !isEmptyList(list);
    }

    public static boolean isEmptyList(List list){
        if(list == null || list.isEmpty() || list.size()==0){
            return true;
        }
        return false;
    }

    public static String[] list2Arr(List<String> list){
        if(isEmptyList(list)){
            return new String[1];
        }
        String[] arr = new String[list.size()];
        for (int i = 0; i < list.size( ); i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    public static boolean checkIsImg(String fileName){
        if(StringUtils.isBlank(fileName)){
            return false;
        }

        String s = fileName.substring(fileName.lastIndexOf(".")+1);
        if(s.equals("png")||
                s.equals("jpg")||
                s.equals("jpeg")||
                s.equals("jpe")||
                s.equals("gif")||
                s.equals("bmp")){
            return true;
        }
        return false;
    }

    // 数字
    private static final String REG_NUMBER = ".*\\d+.*";
    // 小写字母
    private static final String REG_UPPERCASE = ".*[A-Z]+.*";
    // 大写字母
    private static final String REG_LOWERCASE = ".*[a-z]+.*";
    // 特殊符号
    private static final String REG_SYMBOL = ".*[~!@#$%^&*()_+|<>,.?/:;'\\[\\]{}\"]+.*";

    public static boolean checkPasswordRule(String password){
        //密码为空或者长度小于8位则返回false
        if (StringUtils.isBlank(password) || password.length() < 12 ) return false;
        int i = 0;
        if (password.matches(REG_NUMBER)) i++;
        if (password.matches(REG_LOWERCASE))i++;
        if (password.matches(REG_UPPERCASE)) i++;
        if (password.matches(REG_SYMBOL)) i++;

        return i  >= 3;
    }
    public static String getUserDefaultPass(String phone,String spell, String profile){
        String defaultPass = spell.toLowerCase();
        if(Constant.ENV_PROD_KEY.equals(profile)) {
            defaultPass = "QAZ" + phone + "^" + String.valueOf(defaultPass.charAt(0)).toUpperCase( ) + defaultPass.substring(1, defaultPass.length( ));
        }else {
            defaultPass = "#" + phone + "." + String.valueOf(defaultPass.charAt(0)).toUpperCase( ) + defaultPass.substring(1, defaultPass.length( ));
        }
        return defaultPass;
    }
    // 服务器ip
    public static String getHostAddress(){
        try {
            InetAddress ip = InetAddress.getLocalHost();
//            System.out.println("第二种方式"+ip.getHostAddress());
            return ip.getHostAddress();
        }catch (Exception e){
            return null;
        }
    }
    public static void main(String[] args) {
        System.out.println(formatDate("2021-06-04"));
        System.out.println(formatDate("2021-6-4"));
//        System.out.println(getHostAddress());
//
//        System.out.println(countWorkDay(LocalDateTime.parse("2021-04-01 00:00:00",DateUtil.dateTimeFormatter ), LocalDateTime.parse("2021-04-06 00:00:00",DateUtil.dateTimeFormatter )));
//        System.out.println(countWorkDay(LocalDateTime.parse("2021-04-01 00:00:00",DateUtil.dateTimeFormatter ), LocalDateTime.parse("2021-04-01 00:00:00",DateUtil.dateTimeFormatter )));
    }



    public static String formatDate(String str){
        String[] arr = str.split("-");
        if(arr.length==3){
            if(arr[0].length()==2){
                str="20"+arr[0];
            }else {
                str=arr[0];
            }
            int i = Integer.parseInt(arr[1]);
            if(i <10 && arr[1].length()==1){
                str=str+"-0"+arr[1];
            }else {
                str=str+"-"+arr[1];
            }
            i = Integer.parseInt(arr[2]);
            if(i < 10 && arr[2].length()==1){
                str=str+"-0"+arr[2];
            }else {
                str=str+"-"+arr[2];
            }
        }
        return str;
    }

    public static int countWorkDay(LocalDateTime start,LocalDateTime end){
        int i = 0;
        if(start != null && end != null && end.isAfter(start)){
            String endDayStr=end.format(DateUtil.dateFormatter);
            while (!start.format(DateUtil.dateFormatter).equals(endDayStr)){
                start = start.plusDays(1L);
                int weekDay=start.getDayOfWeek().getValue();
                if(weekDay==6 || weekDay ==7){
                    continue;
                }
                i += 1;
            }
        }
        return i;
    }


}
