package com.leo.cloud.common.utils;
//
//import com.alibaba.fastjson.JSON;
//import io.jsonwebtoken.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import javax.crypto.spec.SecretKeySpec;
//import javax.xml.bind.DatatypeConverter;
//import java.security.Key;
//import java.util.Date;
//import java.util.concurrent.TimeUnit;
//
///**
// * 文件名：JwtTokenUtil.java
// * @create 2019-11-25 16:24
// * @create by Dibbing pan
// *
// * @Description: 解析jwt 工具类
// */
//public class JwtTokenUtil {
//    private static Logger log = LoggerFactory.getLogger(JwtTokenUtil.class);
//
//    public static final String AUTH_HEADER_KEY = "Authorization";
//
//    public static final String TOKEN_PREFIX = "AuthParam:";
//
//    public static final String REQUEST_USER_ID_KEY="userId";
//
//    public static final String CLAIM_UUID_KEY="uuid";
//
//    /**
//     * 解析jwt
//     * @param jsonWebToken
//     * @param audience
//     * @return
//     */
//    public static Claims parseJWT(String jsonWebToken, Audience audience) {
//        try {
//            Claims claims = Jwts.parser()
//                    .setSigningKey(DatatypeConverter.parseBase64Binary(audience.getBase64Secret()))
//                    .parseClaimsJws(jsonWebToken).getBody();
////            log.info("-------过期时间---------->"+DateUtil.formatDateTime(claims.getExpiration()));
//            return claims;
//        } catch (ExpiredJwtException  eje) {
//            log.error("===== Token过期 =====", eje);
//            throw new MyException(ResultCode.PERMISSION_TOKEN_EXPIRED);
//        } catch (Exception e){
//            log.error("===== token解析异常 =====", e);
//            throw new MyException(ResultCode.PERMISSION_TOKEN_INVALID);
//        }
//    }
//
//    /**
//     * 构建jwt 并缓存到redis
//     * @param uuidUserName uuid 用户信息
//     * @param audience
//     * @return
//     */
//    public static String createAndCacheJWT(UuidUserName uuidUserName, Audience audience, CacheService cacheService) {
//        try {
//            // 使用HS256加密算法
//            SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
//
//            long nowMillis = System.currentTimeMillis();
//            Date now = new Date(nowMillis);
//
//            //生成签名密钥
//            byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(audience.getBase64Secret());
//            Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
//
//            //添加构成JWT的参数
//            JwtBuilder builder = Jwts.builder().setHeaderParam("typ", "JWT")
//                    // 可以将基本不重要的对象信息放到claims
//                    .claim(CLAIM_UUID_KEY, uuidUserName.getUuid())
//                    .setSubject(uuidUserName.getUsername())           // 代表这个JWT的主体，即它的所有人
//                    .setIssuer(audience.getClientId())              // 代表这个JWT的签发主体；
//                    .setIssuedAt(new Date())        // 是一个时间戳，代表这个JWT的签发时间；
//                    .setAudience(audience.getName())          // 代表这个JWT的接收对象；
//                    .signWith(signatureAlgorithm, signingKey);
//            //添加Token过期时间
//            int TTLMillis = audience.getExpires();
//            if (TTLMillis >= 0) {
//                if(TimeUnit.HOURS.equals(audience.getExpUnit())){
//                    TTLMillis = TTLMillis*60*60*1000;
//                }else if(TimeUnit.MINUTES.equals(audience.getExpUnit())){
//                    TTLMillis = TTLMillis*60*1000;
//                }else if(TimeUnit.SECONDS.equals(audience.getExpUnit())){
//                    TTLMillis = TTLMillis*1000;
//                }else if(TimeUnit.DAYS.equals(audience.getExpUnit())){
//                    TTLMillis = TTLMillis*24*60*60*1000;
//                }
//
//                long expMillis = nowMillis + TTLMillis;
//
//                cacheService.set(uuidUserName.getUuid(), JSON.toJSONString(uuidUserName), (long) TTLMillis);
//                Date exp = new Date(expMillis);
//                builder.setExpiration(exp)  // 是一个时间戳，代表这个JWT的过期时间；
//                        .setNotBefore(now); // 是一个时间戳，代表这个JWT生效的开始时间，意味着在这个时间之前验证JWT是会失败的
//            }
//            //生成JWT
//            return builder.compact();
//        } catch (Exception e) {
//            log.error("签名失败", e);
//            throw new MyException(ResultCode.PERMISSION_SIGNATURE_ERROR);
//        }
//    }
//    /**
//     * 从token中获取用户名
//     * @param token
//     * @param audience
//     * @return
//     */
//    public static String getUsername(String token,Audience audience){
//        return parseJWT(token, audience).getSubject();
//    }
//
//    /**
//     * 是否已过期
//     * @param token
//     * @param audience
//     * @return
//     */
//    public static boolean isExpiration(String token, Audience audience) {
//        return parseJWT(token, audience).getExpiration().before(new Date());
//    }
//}
